import config from 'react-global-configuration';

if(window.location.hostname === 'localhost')
{
    config.set({ 
        apiurl: 'http://192.168.1.109:3000/',
        version: '0.3'
      });
}
else
config.set({ 
    apiurl: 'http://192.168.103.96:3000/',
    version: '0.3'
  });