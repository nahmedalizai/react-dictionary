import React from 'react'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import axios from 'axios'
import config from 'react-global-configuration'
import { useAlert } from 'react-alert'

export default function SuggestForm() {
    const reactAlert = useAlert()
    const [open, setOpen] = React.useState(false);
    const [isRequired, setIsRequired] = React.useState(true)
    const [suggestion, setSuggestion] = React.useState({
        abbreviation:'',
        terminology:'',
        description:'',
        email:''
    });
    function handleClickOpen() {
        setOpen(true)
    }
    
      function handleClose() {
        resetForm()
    }

    function resetForm() {
        setOpen(false)
        setIsRequired(true)
        setSuggestion({
            abbreviation:'',
            terminology:'',
            description:'',
            email:''
        })
    }

    function handleChange(event) {
        if(event.target.name === 'abbreviation') {
            setSuggestion({...suggestion, abbreviation:event.target.value})
            setIsRequired(true)
        }
        else if(event.target.name === 'terminology') {
            setSuggestion({...suggestion, terminology:event.target.value})
            setIsRequired(false)
        }
        else if(event.target.name === 'description')
            setSuggestion({...suggestion, description:event.target.value})
        else if(event.target.name === 'email')
            setSuggestion({...suggestion, email:event.target.value})
    }

    function handleSubmit(event) {
        event.preventDefault()
        var bodyFormData = new FormData();
        bodyFormData.set('method', 'suggestterm');
        axios({
            method: 'post',
            url: config.get('apiurl')+'suggestterm',
            data: bodyFormData,
            params: {
              abrev: suggestion.abbreviation, 
              term: suggestion.terminology,
              description: suggestion.description,
              email: suggestion.email,
              username: ''
            },
            config: { headers: {'Content-Type': 'multipart/form-data' }}
            })
            .then(function (response) {
                if(response != null && response.status === 201 && response.statusText === 'Created') {
                    resetForm()
                    reactAlert.success('Thank you for your suggestion. It will be completed by a panel of subject matters experts. After validation it will be published and become visible in the dictionary.')
            }       
            })
            .catch(function (response) {
                resetForm()
                reactAlert.error('Oops :( Something went wrong. Please try again later.')
            });
        console.log(suggestion)
    }

    return (<div>
        <Button variant="contained" color="default" onClick={handleClickOpen}>Suggest</Button>
        <Dialog fullWidth open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
            <DialogTitle>Please fill below</DialogTitle>
            <DialogContent>
                <form onSubmit={(event) => handleSubmit(event)}>
                    <div className="input-group mb-3">
                        <input className="form-control" type="text" required={isRequired} name="abbreviation" placeholder="Abbreviation" value={suggestion.abbreviation} onChange={handleChange} />
                        <div className="input-group-prepend" style={{marginTop:'8px'}}>
                            <span data-toggle='tooltip' title='Either Abbreviation or Terminology must be filled' className="input-group-text" id="basic-addon1">?</span>
                        </div>
                    </div>
                    <div className="input-group mb-3">
                    <input className="form-control" type="text" required={!isRequired} name="terminology" placeholder="Terminology" value={suggestion.terminology} onChange={handleChange} />
                        <div className="input-group-prepend" style={{marginTop:'8px'}}>
                            <span data-toggle='tooltip' title='Either Abbreviation or Terminology must be filled' className="input-group-text" id="basic-addon1">?</span>
                        </div>
                    </div>
                
                <textarea type="text" name="description" placeholder="Description (optional)" value={suggestion.description} onChange={handleChange} />             
                <div className="input-group mb-3">
                <input className="form-control" type="email" required name="email" placeholder="someone@example.com" value={suggestion.email} onChange={handleChange} />
                        <div className="input-group-prepend" style={{marginTop:'8px'}}>
                            <span data-toggle='tooltip' title='Start with capitals' className="input-group-text" id="basic-addon1">?</span>
                        </div>
                    </div>
                <DialogActions>
                    <Button type='submit' color="primary">Submit</Button>
                    <Button type='button' onClick={handleClose} color="primary">Cancel</Button>
                </DialogActions>
                </form>
            </DialogContent>
        </Dialog>
    </div>)
}