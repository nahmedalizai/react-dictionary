import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import axios from 'axios';
import config from 'react-global-configuration';

export default function SearchFormDialog() {
  const [open, setOpen] = React.useState(false);
  const [search, setSearch] = React.useState({text:''});
  const [data, setData] = React.useState([createData('','','','')]);

  function createData(id, abbreviation, terminology, description) {
    return {id, abbreviation, terminology, description};
  }

  function handleClickOpen() {
    setOpen(true);
  }

  function handleClose() {
    setOpen(false);
    setSearch({text:''})
    setData([createData('','','')]);
  }

  function handleSearch() {
    if(search.text !== '')
    {
      var bodyFormData = new FormData();
        bodyFormData.set('method', 'getinactivesuggestionbyvalue');
        axios({
            method: 'get',
            url: config.get('apiurl')+'getinactivesuggestionbyvalue',
            data: bodyFormData,
            params: {
              value: search.text
            },
            config: { headers: {'Content-Type': 'application/json'}}
          })
          .then(function (response) {
            if(response != null && response.status === 200){
                if(response.data.length > 0)
                   setData(response.data)
            }
          })
          .catch(function (response) {
                alert(response);
          });
    }
  }

  const _tableRows = data.map(function(element) {
    return <TableRow key={element.id}>
            <TableCell>{element.vrp_abbrev}</TableCell>
            <TableCell>{element.vrp_term}</TableCell>
            <TableCell>{element.vrp_description}</TableCell>
        </TableRow>
  });

  const handleChange = event => {
      setSearch({text: event.target.value})
  }

  return (
    <div>
      <Button variant="outlined" color="primary" onClick={handleClickOpen}>search suggestions</Button>
      <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Inactive Suggestions</DialogTitle>
            <DialogContent>
            <TextField
                autoFocus
                margin="dense"
                id="name"
                value={search.text}
                onChange={event => handleChange(event)}
                placeholder="Abbreviation/Terminology"
                fullWidth
            />
            </DialogContent>
            <Paper>
        <Table size="small">
          <TableHead>
            <TableRow>
              <TableCell>Abbreviation</TableCell>
              <TableCell>Terminology</TableCell>
              <TableCell>Description</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
              {_tableRows}
          </TableBody>
        </Table>
      </Paper>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button onClick={handleSearch} color="primary">
            Search
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
