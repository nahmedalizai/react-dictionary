import React, {useEffect} from 'react';
import MaterialTable from 'material-table';
import LinearProgress from '@material-ui/core/LinearProgress';
import './Style.css';
import config from 'react-global-configuration';

function List(props) {
  const [state, setState] = React.useState({
    isLoading: false, 
    error: null,
    items: []
  })
  function getTerms() {
    setState({...state, isLoading: true})
    fetch(config.get('apiurl')+"terms")
      .then(res => res.json())
      .then(
        (result) => {
          setState({...state,items: result, isLoading: false}); 
        },
        (error) => {
          setState({...state, error:error });
        }
      )
  }
  useEffect(() => {
    getTerms() // eslint-disable-next-line
  }, [])

    return (
      <div>
        {state.isLoading && <LinearProgress color="secondary" />}
        <MaterialTable
          columns={[
            { title: 'Abbreviation', field: 'vrp_abv' },
            { title: 'Terminology', field: 'vrp_term' },
            { title: 'Description', field: 'vrp_description' },
            //{ title: 'Category', field: '', lookup: { 1: 'Business', 2: 'Veripark' } },
            { title: 'Category', field: 'vrp_categoryname'}
          ]}
          options={{
            //module material-module/dist/defaults-props contains all supported properties
            selection: false,
            filtering: false,
            sorting: true,
            pageSize: 50,
            pageSizeOptions: [100, 150, 200],
            emptyRowsWhenPaging: false,
            searchFieldAlignment: 'left',
            showTitle: true
          }}
          data= {state.items}
          title={<span style={{fontSize:'0.9rem'}}>(Not case sensitive)</span>}
        />
      </div>
    )
}

export default List;