import React from 'react'
import './Style.css'
import axios from 'axios'
import config from 'react-global-configuration';
class Form extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        abrev: '',
        term: '',
        desc: '',
        email: '',
        message: '',
        isSuccess: false
      };
  
      this.handleChangeAbrev = this.handleChangeAbrev.bind(this);
      this.handleChangeTerm = this.handleChangeTerm.bind(this);
      this.handleChangeDesc = this.handleChangeDesc.bind(this);
      this.handleChangeEmail = this.handleChangeEmail.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
    }
  
    handleChangeAbrev(event) {
      this.setState({abrev: event.target.value});
    }

    handleChangeTerm(event) {
      this.setState({term: event.target.value});
    }

    handleChangeDesc(event) {
      this.setState({desc: event.target.value});
    }

    handleChangeEmail(event) {
      this.setState({email: event.target.value});
    } 
  
    handleSubmit(event) {
      var self = this;
      var bodyFormData = new FormData();
      bodyFormData.set('method', 'suggestterm');
      if(this.state.abrev === "" && this.state.term === "")
        alert("Either Abbreviation or Terminology is required")
      else {
        axios({
        method: 'post',
        url: config.get('apiurl')+'suggestterm',
        data: bodyFormData,
        params: {
          abrev: this.state.abrev, 
          term: this.state.term,
          description: this.state.desc,
          email: this.state.email,
          username: ''
        },
        config: { headers: {'Content-Type': 'multipart/form-data' }}
        })
        .then(function (response) {
            if(response != null && response.status === 201 && response.statusText === 'Created') {
              self.setState({isSuccess: true});
              self.setState({message: 'Thank you! This will be added soon in the dictionary.'});
        }       
        })
        .catch(function (response) {
            self.setState({isSuccess: true});
            self.setState({message: 'Oops :( Something went wrong. Please try again later.'});
        });
      }
      event.preventDefault();
    }
  
    render() {
      const { abrev, term, desc, email,message, isSuccess} = this.state;
      let form = (        
      <form onSubmit={this.handleSubmit}>
        <label>Please fill below</label>
          <input type="text" name="abrev" placeholder="Abbreviation" value={abrev} onChange={this.handleChangeAbrev} />
          <input type="text" name="term" placeholder="Terminology" value={term} onChange={this.handleChangeTerm} />
          <textarea type="text" name="desc" placeholder="Description" value={desc} onChange={this.handleChangeDesc} />             
          <input type="email" name="email" placeholder="someone@example.com" required value={email} onChange={this.handleChangeEmail} />
          <div class="form-group">
              <input class="form-control" type="submit" value="Submit" />
            </div>
      </form>);
      console.log('From render : ' + isSuccess);
      if(isSuccess)
        form = (<div>{message}</div>);
      return (
        <div>{form}</div>
      );
    }
  }

  export default Form