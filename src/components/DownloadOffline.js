import React from 'react';
import axios from 'axios';
import config from 'react-global-configuration';

export default function DownloadOffline() {

    function buildTable(data) {
        var table = document.createElement("table");
        var thead = document.createElement("thead");
        var tbody = document.createElement("tbody");
        var headRow = document.createElement("tr");
        ["Abbreviation","Terminology","Description","Category"].forEach(function(el) {
            var th=document.createElement("th");
            th.style.border = '1px solid';
            th.style.textAlign = 'left';
            th.appendChild(document.createTextNode(el));
            headRow.appendChild(th);
          });
        thead.appendChild(headRow);
        table.appendChild(thead); 
        data.forEach(function(el) {
          var tr = document.createElement("tr");
          for (var o in el) {  
            var td = document.createElement("td");
            td.style.border = '1px solid';
            td.style.textAlign = 'left';
            td.appendChild(document.createTextNode(el[o]))
            tr.appendChild(td);
          }
          tbody.appendChild(tr);  
        });
        table.appendChild(tbody);             
        return table;
    }

    function downloadOfflineClick() {
      var bodyFormData = new FormData();
        bodyFormData.set('method', 'terms');
        axios({
            method: 'get',
            url: config.get('apiurl')+'terms',
            data: bodyFormData,
            config: { headers: {'Content-Type': 'application/json'}}
          })
          .then(function (response) {
            if(response != null && response.status === 200){
              var atable = buildTable(response.data);
              atable.id = "dataTable";
              atable.style.fontFamily = 'arial, sans-serif';
              atable.style.borderCollapse = 'collapse';
              atable.style.width = '100%';
              document.body.appendChild(atable);
              var htmlContent = [document.getElementById('dataTable').outerHTML];
              var bl = new Blob(htmlContent, {type: "text/html"});
              var a = document.createElement("a");
              a.href = URL.createObjectURL(bl);
              a.download = "lexicon.html";
              a.hidden = true;
              document.body.appendChild(a);
              a.innerHTML = "";
              a.click();
              document.body.removeChild(atable);
            }
          })
          .catch(function (response) {
                alert(response);
          });
    }

    return (
        <React.Fragment>
        <button className='tileButton btn-success col-2' onClick={downloadOfflineClick}>Download Offline</button>
        </React.Fragment>
    );
}