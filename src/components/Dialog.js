import React, { Component } from "react";
import './Style.css';
import Form from './Form';
class Dialog extends Component {
    state ={
        isSuccess: false
    }
    render() {
        let form = (
            <Form/>
        );
        let dialog = (
            <div className="popup">
                <div>{form}</div>
                <button  className="closeButton btn btn-danger btn-xs" onClick={this.props.onClose}>x</button>
                <div>
                    {this.props.children}
                </div> 
            </div>
        );

        if(! this.props.isOpen) {
            dialog = null;
        }
        return (
            <div>{dialog}</div>
        )
    }
}

export default Dialog;
