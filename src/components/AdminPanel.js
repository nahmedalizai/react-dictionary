import React from 'react'
import MaterialTable from 'material-table'
import axios from 'axios'
import './Style.css'
import config from 'react-global-configuration'
import SearchFormDialog from './SearchFormDialog'
import DownloadOffline from './DownloadOffline'
import UserManagement from './UserManagement'

export default function AdminPanel() {
  const [isValidated, setIsValidated] = React.useState(false);
  const [username, setUsername] = React.useState('');
  const [password, setPassword] = React.useState('');
  const [editRowId, setEditRowId] = React.useState(null);
  const [abbreviation,setAbbreviation] = React.useState('');
  const [terminology,setTerminology] = React.useState('');
  const [description,setDescription] = React.useState('');
  const [selectedCategory, setSelectedCategory] = React.useState('');
  const [edit, setEdit] = React.useState(true);
  const [manage, setManage] = React.useState(null);
  const [add, setAdd] = React.useState(false);
  const [disabled, setDisabled] = React.useState(true);
  const [search, setSearch] = React.useState({text: ''});
  const [errorMessage, setErrorMessage] = React.useState('');
  const [state, setState] = React.useState({
    columns: [
      { title: 'Abbreviation', field: 'vrp_abbrev'},
      { title: 'Suggested Term', field: 'vrp_term' },
      { title: 'Description', field: 'vrp_description' },
      { title: 'Suggested by', field: 'vrp_email'},
      {
        title: 'Category',
        field: 'vrp_categoryid',
        lookup: { 0: 'Unknown', 1: 'VeriPark', 2: 'Business', 3: 'Technology', 4:'Brand'},
      },
    ],
    data: [],
  });
  
  // useEffect(() => {
  // }, []);

  function searchButtonClick() {
    if(search.text !== '')
    {
      var bodyFormData = new FormData();
        bodyFormData.set('method', 'gettermbyvalue');
        axios({
            method: 'get',
            url: config.get('apiurl')+'gettermbyvalue',
            data: bodyFormData,
            params: {value: search.text},
            config: { headers: {'Content-Type': 'application/json'}}
          })
          .then(function (response) {
            if(response != null && response.status === 200){
              if(response.data.length > 0)
              {
                setEditRowId(response.data[0].id)
                setAbbreviation(response.data[0].vrp_abv)
                setTerminology(response.data[0].vrp_term)
                setDescription(response.data[0].vrp_description)
                setSelectedCategory(response.data[0].vrp_categoryid)
                setDisabled(false)
              }
              else {
                setErrorMessage('No record found');
                resetUpdateFrom();
              }
            }
          })
          .catch(function (response) {
                alert(response);
          });
    }
  }

  function onUpdateClick(event) {
    if(editRowId !== null)
    {
      var bodyFormData = new FormData();
        bodyFormData.set('method', 'updatetermbyid');
        axios({
            method: 'post',
            url: config.get('apiurl')+'updatetermbyid',
            data: bodyFormData,
            params: {
              id: editRowId,
              vrp_abv: abbreviation,
              vrp_term: terminology,
              vrp_description: description,
              vrp_categoryid: selectedCategory
            },
            config: { headers: {'Content-Type': 'application/json'}}
          })
          .then(function (response) {
            if(response != null && response.status === 201){
              alert(response.data)
              resetUpdateFrom();
            }
          })
          .catch(function (response) {
                alert(response);
          });
    }
    event.preventDefault();
  }

  function ondeleteClick(event) {
    if(editRowId !== null)
    {
      var bodyFormData = new FormData();
        bodyFormData.set('method', 'deletetermbyid');
        axios({
            method: 'post',
            url: config.get('apiurl')+'deletetermbyid',
            data: bodyFormData,
            params: {
              id: editRowId
            },
            config: { headers: {'Content-Type': 'application/json'}}
          })
          .then(function (response) {
            if(response != null && response.status === 201){
              alert(response.data)
              resetUpdateFrom();
            }
          })
          .catch(function (response) {
                alert(response);
          });
    }
    event.preventDefault();
  }

  function submitSuggestion(data) {
    var message = null;
    if (data != null)
    {
      if(data.vrp_term === undefined || data.vrp_term === null || data.vrp_term === "")
        message = 'Missing information';
      if(data.vrp_abbrev === undefined || data.vrp_abbrev === null || data.vrp_abbrev === "")
        message = 'Missing information';
      if(data.vrp_description === undefined || data.vrp_description === null || data.vrp_description === "")
        message = 'Missing information';
      if(data.vrp_email === undefined || data.vrp_email === null || data.vrp_email === "")
        message = 'Missing information';
      if(data.vrp_categoryid === undefined)
        message = 'Missing information';
      if(message != null)
        alert(message);
      else {
        var bodyFormData = new FormData();
        bodyFormData.set('method', 'addterm');
        axios({
            method: 'post',
            url: config.get('apiurl')+'addterm',
            data: bodyFormData,
            params: {data: data},
            config: { headers: {'Content-Type': 'application/json'}}
          })
          .then(function (response) {
            if(response != null && response.status === 202)
                  alert(response.data);
            else if(response != null && response.status === 201)
                  getSuggestions()
          })
          .catch(function (response) {
                alert(response);
          });
      }
    }
    else 
      alert('No data to submit');
  }

  function deleteSuggestion(rowData) {
    if(rowData.id){
      var bodyFormData = new FormData();
      bodyFormData.set('method', 'rejectSuggestion');
      axios({
          method: 'post',
          url: config.get('apiurl')+'rejectsuggestion',
          data: bodyFormData,
          params: {id: rowData.id},
          config: { headers: {'Content-Type': 'application/json'}}
        })
        .then(function (response) {
          if(response != null && response.status === 201)
          {
            getSuggestions();
            alert('Suggestion rejected')
          }
        })
        .catch(function (response) {
              alert(response);
        });
    }
    getSuggestions();
  }

  function setStateToRender(result) {
    const data = result.data;
    setState({ ...state, data });
  };

  function getSuggestions() {
    const fetchData = async () => {
      const result = await axios(
        config.get('apiurl')+'getsuggestions',
      );
      setStateToRender(result)
    };
    fetchData();
  }
  
  function manageSuggestions() {
    return (
        <div className='materialTableContainer'>
        <label>Manage Suggestions</label>
          <SearchFormDialog/>
          <MaterialTable
            title=""
            columns={state.columns}
            data={state.data}
            options={{
                search: false,
            }}
            actions={[
              {
                icon: 'save',
                tooltip: 'submit',
                onClick: (event, rowData) => {
                  submitSuggestion(rowData);
                }

              },
              {
                icon: 'delete',
                tooltip: 'delete',
                onClick: (event, rowData) => {
                  deleteSuggestion(rowData);
                }
                
              }
            ]}
            editable={{
              onRowAdd: newData =>
                new Promise(resolve => {
                  setTimeout(() => {
                    resolve();
                    const data = [...state.data];
                    data.push(newData);
                    setState({ ...state, data });
                  }, 600);
                })
                ,
              onRowUpdate: (newData, oldData) =>
              new Promise(resolve => {
                  setTimeout(() => {
                    resolve();
                    const data = [...state.data];
                    data[data.indexOf(oldData)] = newData;
                    setState({ ...state, data });
                  }, 600);
                }),
            }}
          />
        </div>
    )
  }

  function handleAbbreviationChange(event) {
    setAbbreviation(event.target.value)
  }

  function handleTerminologyChange(event) {
    setTerminology(event.target.value)
  }

  function handleDescriptionChange(event) {
    setDescription(event.target.value)
  }

  function handleSearchChange(event) {
    setSearch({text: event.target.value})
    setErrorMessage('');
  }

  function handleCategoryChange(event) {
    setSelectedCategory(event.target.value)
  }

  function handleUsernameChange(event) {
    setUsername(event.target.value)
  }

  function handlePasswordChange(event) {
    setPassword(event.target.value)
  }

  function resetUpdateFrom() {
    setAbbreviation('')
    setTerminology('')
    setDescription('')
    setSelectedCategory('')
    setDisabled(true)
  }

  function onResetClick(event) {
    resetUpdateFrom();
    event.preventDefault();
  }

  function editDictionary() {
    return (
      <div>
        <div className='searchContainer'>
          <label>Edit Dictionary</label><br/>
          <input className='col-5 form-control-sm' type="text" placeholder="Search by Abbreviation/Terminology" onChange={(event) => handleSearchChange(event)}/>
          <button className='searchButton btn-sm' onClick= {() => searchButtonClick()}>Search</button>
          <br/><span style={{color: 'red'}}>{errorMessage}</span>
        </div>
        <div className='editFormContainer'>
          <form className='editForm'>
            <input disabled={disabled} type='text' placeholder='Abbreviation' value={abbreviation} onChange={(event) => handleAbbreviationChange(event)}/>
            <input disabled={disabled} type='text' placeholder='Terminology' value={terminology} onChange={(event) => handleTerminologyChange(event)}/>
            <textarea disabled={disabled} type='text' placeholder='Description' value={description} onChange={(event) => handleDescriptionChange(event)}/>
            <select disabled={disabled} value={selectedCategory} onChange={(event) => handleCategoryChange(event)}>
              <option value=""> -- select an option -- </option>
              <option value="0">Unknown</option>
              <option value="1">VeriPark</option>
              <option value="2">Business</option>
              <option value="3">Technology</option>
              <option value="4">Brand</option>
            </select>
            <button disabled={disabled} type='submit' className='editFormButtons btn btn-success' onClick={(event) => onUpdateClick(event)}>Update</button>
            <button disabled={disabled} className='editFormButtons btn btn-danger' onClick={(event) => ondeleteClick(event)}>Delete</button>
            <button type='reset' className='editFormButtons btn btn-info' onClick={(event) => onResetClick(event)}>Clear</button>
          </form>
        </div>
      </div>
    )
  }

  function manageUserClick() {
    if(add)
      setAdd(false);
    else
    {
      setAdd(true);
      setEdit(false);
      setManage(false);
    } 
  }

  function manageSuggestionsClick() {
    if(manage)
      setManage(false);
    else
    {
      getSuggestions();
      setManage(true);
      setAdd(false);
      setEdit(false);
    }   
  }

  function editDictionaryClick() {
    if(edit)
      setEdit(false);
    else
    {
      setAdd(false);
      setEdit(true);
      setManage(false);
    }
  }

  function onLoginClick(event) {
      var bodyFormData = new FormData();
        bodyFormData.set('method', 'login');
        axios({
            method: 'get',
            url: config.get('apiurl')+'login',
            data: bodyFormData,
            params: {
              username: username,
              password: password
            },
            config: { headers: {'Content-Type': 'application/json'}}
          })
          .then(function (response) {
            if(response != null && response.status === 200 && response.data.isSuccess){
              setIsValidated(true)
            }
            else if(response != null && response.status === 202 && !response.data.isSuccess){
              alert('Wrong username or password')
            }
          })
          .catch(function (response) {
                alert(response);
          });
    event.preventDefault();
  }

  function dashboard() {
    return <div>
      <div id='tiles' className='tilesContainer'>
        <button className='tileButton btn-warning col-2' onClick={editDictionaryClick}>Edit Dictionary</button>
        <button className='tileButton btn-primary col-2' onClick={manageSuggestionsClick}>Manage Suggestions</button>
        <button className='tileButton btn-info col-2' onClick={manageUserClick}>User Management</button>
        <DownloadOffline/>
      </div>
        {manage && manageSuggestions()}
        {edit && editDictionary()}
        {add && <UserManagement/>}
    </div>
  }

  function login() {
    return <div style={{textAlign:'center'}}>
            <div className='loginContainer'>
                  <form className='editForm' onSubmit={(event) => onLoginClick(event)}>
                    <div className="form-group">
                      <input className="form-control" type='text' required placeholder='Username' value={username} onChange={(event) => handleUsernameChange(event)}/>
                    </div>
                    <div className="form-group">
                      <input className="form-control" type='password' required placeholder='Password' value={password} onChange={(event) => handlePasswordChange(event)}/>
                    </div>
                    <div className="form-group">
                      <input className="form-control" type="submit" value="Login" />
                    </div>
                    <span style={{fontSize:'small', fontFamily: 'inherit'}}><b>{'Version : ' + config.get('version')}</b></span>
                  </form>
            </div>
      </div>
  }

  return (
    <div>
      {!isValidated && login()}
      {isValidated && dashboard()}
    </div>
  );
}