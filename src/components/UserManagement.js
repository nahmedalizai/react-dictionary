import React from 'react'
import axios from 'axios'
import config from 'react-global-configuration'

export default function UserManagement(prop) {
    const [disabled, setDisabled] = React.useState(true)
    const [user_Name, setUser_Name] = React.useState('');
    const [user_Username, setUser_Username] = React.useState('');
    const [user_Password, setUser_Password] = React.useState('');
    const [search, setSearch] = React.useState('');
    const [editUser, setEditUser] = React.useState({
        id:null,
        username:'',
        password:''
    });

    const handlechange = event => {
        if(event.target.name === 'name')
            setUser_Name(event.target.value)
        else if(event.target.name === 'username')
            setUser_Username(event.target.value)
        else if(event.target.name === 'password')
            setUser_Password(event.target.value)
        else if(event.target.name === 'search')
            setSearch(event.target.value)
        else if(event.target.name === 'editpassword')
            setEditUser({...editUser,password: event.target.value})

      }

    function onAddClick(event) {
        var bodyFormData = new FormData();
          bodyFormData.set('method', 'adduser');
          axios({
              method: 'post',
              url: config.get('apiurl')+'adduser',
              data: bodyFormData,
              params: {
                vrp_name: user_Name,
                vrp_username: user_Username,
                vrp_password: user_Password
              },
              config: { headers: {'Content-Type': 'application/json'}}
            })
            .then(function (response) {
              if(response != null){
                alert(response.data)
                setUser_Name('')
                setUser_Username('')
                setUser_Password('')
              }
            })
            .catch(function (response) {
                  alert(response);
            });
            event.preventDefault();
    }

    function handleSearchClick() {
      setEditUser({
        id:null,
        username:'',
        password:''
      })
      setDisabled(true)
      if(search !== undefined && search !== null && search !== '') {
        var bodyFormData = new FormData();
        bodyFormData.set('method', 'getuserbyusername');
        axios({
            method: 'get',
            url: config.get('apiurl')+'getuserbyusername',
            data: bodyFormData,
            params: {username: search},
            config: { headers: {'Content-Type': 'application/json'}}
          })
          .then(function (response) {
            if(response != null && response.status === 200)
                if(response.data.length > 0)
                {
                  setEditUser({...editUser,id:response.data[0].id, username: response.data[0].vrp_username})
                  setDisabled(false)
                }
                else 
                  alert('user not found')
          })
          .catch(function (response) {
                alert(response);
          });
      }
    }

    function changePassword(event) {
      if(editUser.password !== null && editUser.password !== '' && editUser.id !== null) {
        var bodyFormData = new FormData();
        bodyFormData.set('method', 'changeuserpassword');
        axios({
            method: 'post',
            url: config.get('apiurl')+'changeuserpassword',
            data: bodyFormData,
            params: {
              id: editUser.id,
              password: editUser.password
            },
            config: { headers: {'Content-Type': 'application/json'}}
          })
          .then(function (response) {
            if(response != null && response.status === 201){
                alert(response.data)
                setEditUser({
                  id:null,
                  username:'',
                  password:''
                })
                setDisabled(true)
            }
            else 
              alert(response.data)
          })
          .catch(function (response) {
                alert(response);
          });
      }
      event.preventDefault()
    }
    
    function deleteUserClick() {
      if(editUser.id !== null) {
        var bodyFormData = new FormData();
        bodyFormData.set('method', 'deleteuser');
        axios({
            method: 'post',
            url: config.get('apiurl')+'deleteuser',
            data: bodyFormData,
            params: {
              id: editUser.id
            },
            config: { headers: {'Content-Type': 'application/json'}}
          })
          .then(function (response) {
            if(response != null && response.status === 201){
                alert(response.data)
                setEditUser({
                  id:null,
                  username:'',
                  password:''
                })
                setDisabled(true)
                setSearch('')
            }
            else 
              alert(response.data)
          })
          .catch(function (response) {
                alert(response);
          });
      }
    }

    return (
        <div>
        <label className='labelMargin'>User Management</label><br/>
        <div>
          <div className='col'>
          <div className='editFormContainer'>
            <form className='editForm' onSubmit={(event) => onAddClick(event)}>
                <div className="form-group">
                  <input name='name' className="form-control" required type='text' placeholder='Name' value={user_Name} onChange={handlechange}/>
                </div>
                <div className="form-group">
                  <input name='username' className="form-control" required type='text' placeholder='Username' value={user_Username} onChange={handlechange}/>
                </div>
                <div className="form-group">
                  <input name='password' className="form-control" required type='password' placeholder='Password' value={user_Password} onChange={handlechange}/>
                </div>
                <div className="form-group">
                  <button className="btn btn-success" type="submit" value="Add User">Add User</button>
                </div>
              </form>
            </div>
          </div>
          <div className='col'>
            <div className='editFormContainer'>
                <div className='row' style={{marginTop:'20px'}}>
                    <div className='col'>
                        <div className='form-group' style={{marginLeft:'10px'}}>
                            <input className="form-control" name='search' placeholder='search by username' value={search} onChange={handlechange}/>
                        </div>
                    </div>
                    <div className='col'>
                        <div className='form-group'>
                            <button className='btn btn-primary btn-sm' onClick={handleSearchClick}>Search</button>
                        </div>
                    </div>
                </div>
                <form className='editForm' onSubmit={changePassword}>
                  <fieldset disabled={disabled}>
                  <div className="form-group">
                    <input name='editusername' className="form-control" disabled type='text' placeholder='Username' value={editUser.username} onChange={handlechange}/>
                  </div>
                  <div className="form-group">
                    <input name='editpassword' className="form-control" required type='password' placeholder='Password' value={editUser.password} onChange={handlechange}/>
                  </div>
                  <div className="form-group">
                    <button className='btn btn-info' type='submit' value='Change Password'>Change Password</button>
                    <button style={{marginLeft: '10px'}} className='btn btn-danger' type='button' onClick={deleteUserClick}>Delete User</button>
                  </div>
                </fieldset>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
}