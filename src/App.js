import React, {Component} from 'react';
import { Route, Switch, BrowserRouter as Router } from 'react-router-dom';
import List from './components/List';
import AdminPanel from './components/AdminPanel';
import SuggestForm from './components/SuggestForm'
import {Navbar} from 'react-bootstrap';
import './App.css';
import './config';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
// import logo from './logo.png'

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false
    };
  }
  render() {
    return (
          <div>
            <Navbar fixed='top' className='navbar'>
              {/* <img src={logo}/> */}
              <span className='navbarBrand'>VERIPARK DICTIONARY</span>
              <Navbar.Collapse id="basic-navbar-nav">
              </Navbar.Collapse>
              <SuggestForm/>
            </Navbar>
            <div className='content'>
              <Router>
                  <div>
                    <Switch>
                      <Route exact path="/" component={List} props={this.state} />
                      <Route exact path="/adminpanel" component={AdminPanel} /> 
                    </Switch>
                  </div>
              </Router>
            </div>
          </div>
    );
  }
}
export default App;